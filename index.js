/* Bài 1: Viết 3 số nguyên theo thứ tự tăng dần
- Đầu vào:
Khai báo 3 biến số nguyên 1, 2 3
- Xử lý:
Cho từng trường hợp xuất ra thứ tự tăng dần cho từng trường hợp
- Đầu ra:
Số sắp xếp theo thứ tự tăng dần.
 */
function sapXep() {
  var so1 = document.getElementById("soNguyen1").value;
  var so2 = document.getElementById("soNguyen2").value;
  var so3 = document.getElementById("soNguyen3").value;
  if (so1 < so2 && so2 < so3) {
    thuTu = so1 + so2 + so3;
  } else if (so1 < so2 && so2 > so3 && so3 < 1) {
    thuTu = so1 + so3 + so2;
  } else if (so2 < so1 && so1 < so3) {
    thuTu = so2 + so1 + so3;
  } else if (so2 < so1 && so1 > so3 && so2 < 1) {
    thuTu = so2 + so3 + so1;
  } else if (so3 < so1 && so1 < so2) {
    thuTu = so3 + so1 + so2;
  } else if (so3 < so1 && so1 > so2) {
    thuTu = so3 + so2 + so1;
  } else {
    return;
  }
  document.getElementById("result").innerHTML = `<p">Thứ tự sắp xếp:</p>
    <h2>${thuTu}</h2>`;
}
/*Bài 2: Chào hỏi các thành viên trong gia đình:
+ Đầu vào:
Gán id = tv (thành viên đăng nhập)
+ Xử lý:
B = Bố; M = Mẹ; A= Anh trai; E= Em gái
If từng trường hợp, đúng với kí tự nào thì chào người đó (tv =B || tv = M || tv = A || tv = E)
+ Đầu ra:
Xuất ra màn hình Xin chào thành viên ....*/
function xinChao() {
    var tv = document.getElementById("thanhvien").value;
    var B = document.getElementById("bo").value;
    var M = document.getElementById("me").value;
    var A = document.getElementById("anh").value;
    var E = document.getElementById("em").value;
    if (tv == B) {
        document.getElementById("result2").innerHTML = `<p>Xin Chào Bố</p>`;
    } else if (tv == M) {
        document.getElementById("result2").innerHTML = `<p>Xin Chào Mẹ</p>`;
    } else if (tv == A) {
        document.getElementById("result2").innerHTML = `<p>Xin Chào Anh Trai</p>`;
    } else if (tv == E) {
        document.getElementById("result2").innerHTML = `<p>Xin Chào Em Gái</p>`;
    } else {
        document.getElementById("result2").innerHTML = `<p>Xin Chào Người Lạ</p>`;
    }
}

    /*Bài 3: Đếm chẵn lẻ của 3 số nguyên
    + Đầu vào:
    Gán 3 số cho s1, s2, s3
    + Xử lý: do có 3 trường hợp nên chia làm 3 trường hợp
    3 số đều dương
    2 số dương 
    Trường hợp còn lại
    + Đầu ra: Đếm bao nhiêu số chẵn, số lẻ
     */
    function demSo() {
        var s1 = document.getElementById("so1").value * 1;
        var s2 = document.getElementById("so2").value * 1;
        var s3 = document.getElementById("so3").value * 1;
        if (s1 % 2 == 0 && s2 % 2 == 0 && s3 % 2 == 0) {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 3 số</p><p>Số Lẻ Có: 0 số</p>`;
        } else if (s1 % 2 == 0 && s2 % 2 == 0 && s3 % 2 == 1) {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
        } else if (s1 % 2 == 1 && s2 % 2 == 0 && s3 % 2 == 0) {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
        } else if (s1 % 2 == 0 && s2 % 2 == 1 && s3 % 2 == 0) {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 2 số</p><p>Số Lẻ Có: 1 số</p>`;
        } else if (s1 % 2 == 0 && s2 % 2 == 1 && s3 % 2 == 1) {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 1 số</p><p>Số Lẻ Có: 2 số</p>`;
        } else {
            document.getElementById(
                "result3"
            ).innerHTML = `<p>Số Chẵn Có: 1 số</p><p>Số Lẻ Có: 2 số</p>`;
        }
    }
    /*Bài 4:
    + Đầu vào: 
    Gán 3 cạnh tương ứng: a, b, c
    + Xử lý:
    Kiểm tra 3 cạnh tam giác: 
    a + b <= c;  a + c <= b; b + c <= a
    Rồi đưa công thức tính của từng tam giác: tam giác đều nếu 3 cạnh bằng nhau; tam giác cân nếu 2 cạnh bằng nhau; tam giác vuông nếu: c^2 = a^2 + b^2
     */

    function canhTamGiac() {
        var a = document.getElementById("canh1").value * 1;
        var b = document.getElementById("canh2").value * 1;
        var c = document.getElementById("canh3").value * 1;
        if (a + b <= c || a + c <= b || b + c <= a) {
            document.getElementById(
                "result4"
            ).innerHTML = `<p>Tam giác không hợp lệ, vui lòng nhập lại</p>`;
        } else if (
            a * a + b * b == c * c ||
            a * c + c * c == b * b ||
            b * b + c * c == a * c
        ) {
            document.getElementById("result4").innerHTML = `<p>Tam giác vuông</p>`;
        } else if (a == b && b == c) {
            document.getElementById("result4").innerHTML = `<p>Tam giác đều</p>`;
        } else if (a == b || a == c || b == c) {
            document.getElementById("result4").innerHTML = `<p>Tam giác cân</p>`;
        } else {
            document.getElementById("result4").innerHTML = `<p>Tam giác thường</p>`;
        }
    }
